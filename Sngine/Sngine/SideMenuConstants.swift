//
//  SideMenuConstants.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

struct SideMenuConstants {
    static let darkBackground = UIColor.rgb(59, green: 61, blue: 65)
    static let sideMenuCell = "SideMenuCell"
    static let searchAndSettingsCell = "SearchAndSettingsCell"
}
