//
//  AppController.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 12/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit

class AppController: NSObject {
    
    class func launchDashboardIn(window: UIWindow) {
        window.rootViewController = UINavigationController(rootViewController: LoginViewController())
        window.makeKeyAndVisible()
        setupAppearance()
    }
    
    
    private class func setupAppearance () {
        UINavigationBar.appearance().barTintColor = Colors.facebookDarkBlue
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        UITabBar.appearance().tintColor = Colors.facebookLightBlue
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    
    
}
