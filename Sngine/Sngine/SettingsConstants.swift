//
//  SettingsConstants.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit


struct SettingsCells {
    static let settingsUserCellId = "SettingsUserCell"
    static let settingsCellId = "SettingsCell"
}
