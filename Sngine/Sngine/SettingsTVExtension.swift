//
//  SettingsTVExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

extension SettingsVC: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return settingsArray.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let userCell = tableView.dequeueReusableCell(withIdentifier: SettingsCells.settingsUserCellId, for: indexPath) as! SettingsUserCell
            return userCell
        default:
            let settingsCell = tableView.dequeueReusableCell(withIdentifier: SettingsCells.settingsCellId, for: indexPath) as! SettingsCell
            let item = settingsArray[indexPath.row]
            settingsCell.setupCell(item: item)
            return settingsCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 70
        default:
            return 50
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 0
        default:
            return 25
        }
    }
    
    
    
    
    func registerSettingsUserCell() {
        let nib = UINib(nibName: SettingsCells.settingsUserCellId, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: SettingsCells.settingsUserCellId)
    }
    
    func registerSettingsCell() {
        let nib = UINib(nibName: SettingsCells.settingsCellId, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: SettingsCells.settingsCellId)
    }
    
    
}
