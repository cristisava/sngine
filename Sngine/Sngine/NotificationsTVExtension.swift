//
//  NotificationsTVExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 26/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit


extension NotificationsVC: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let notificationCell = tableView.dequeueReusableCell(withIdentifier: NotificationsCells.notificationCellId, for: indexPath)
        return notificationCell
    }
    
    
    func registerNotificationCell() {
        let notificationCellNib = UINib(nibName: NotificationsCells.notificationCellId, bundle: nil)
        tableView.register(notificationCellNib, forCellReuseIdentifier: NotificationsCells.notificationCellId)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.white
        let fullPostVC = FullPostVC(nibName: "FullPostVC", bundle: nil)
        self.navigationController?.pushViewController(fullPostVC, animated: true)
    }
    
}
