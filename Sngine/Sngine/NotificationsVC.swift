//
//  NotificationsVC.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 26/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class NotificationsVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }

    
    func setupTableView () {
        tableView.delegate = self
        tableView.dataSource = self
        registerNotificationCell()
    }


}
