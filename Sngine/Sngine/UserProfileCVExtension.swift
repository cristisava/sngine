//
//  UserProfileCVExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 29/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit


extension UserProfileVC {
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let userInfoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserInfoCell", for: indexPath) as! UserInfoCell
            return userInfoCell
        default:
            let feedCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellsConstants.feedCellId, for: indexPath) as! FeedCell
            feedCell.delegate = self
            addTapGestureToUserImage(cell: feedCell)
            feedCell.post = posts[indexPath.item]
            return feedCell
        }
        
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.item {
        case 0:
            return CGSize(width: UIScreen.main.bounds.width, height: 500)
        default:
            if let statusText = posts[indexPath.item].statusText {
                
                let rect = NSString(string: statusText).boundingRect(with: CGSize(width: view.frame.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
                
                let knownHeight: CGFloat = 8 + 44 + 4 + 4 + 200 + 8 + 24 + 8 + 44
                
                return CGSize(width: view.frame.width, height: rect.height + knownHeight + 24)
            }
        }
        return CGSize(width: view.frame.width, height: 500)
    }
    
    
    func registerCells() {
        let nib = UINib(nibName: "UserInfoCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "UserInfoCell")
    }
    
    
    
}
