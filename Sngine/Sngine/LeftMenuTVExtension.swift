//
//  LeftMenuTVExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit


extension LeftSlideInMenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return usersList.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let searchAndSettingsCell = tableView.dequeueReusableCell(withIdentifier: SideMenuConstants.searchAndSettingsCell, for: indexPath) as! SearchAndSettingsCell
            return searchAndSettingsCell
        case 1:
            let sideMenuCell = tableView.dequeueReusableCell(withIdentifier: SideMenuConstants.sideMenuCell, for: indexPath) as! SideMenuCell
            let item = usersList[indexPath.row]
            sideMenuCell.setupCell(item: item)
            return sideMenuCell
        default:
            return UITableViewCell()
        }
    }
    
    
    func registerCells() {
        let sideMenuCellNib = UINib(nibName: SideMenuConstants.sideMenuCell, bundle: nil)
        tableView.register(sideMenuCellNib, forCellReuseIdentifier: SideMenuConstants.sideMenuCell)
        let searchAndSettingsCellNib = UINib(nibName: SideMenuConstants.searchAndSettingsCell, bundle: nil)
        tableView.register(searchAndSettingsCellNib, forCellReuseIdentifier: SideMenuConstants.searchAndSettingsCell)
    }
    
    
}
