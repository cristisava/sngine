//
//  Post.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 11/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation


class Post {
    var name: String?
    var profileImageName: String?
    var statusText: String?
    var statusImageUrl: String?
    var numLikes: Int?
    var numComments: Int?
    var isLiked: Bool = false
}
