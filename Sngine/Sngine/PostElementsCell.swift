//
//  PostElementsCell.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 28/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class PostElementsCell: UITableViewCell {

    
    @IBOutlet weak var elementIcon: UIImageView!
    
    @IBOutlet weak var elementName: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCell(element: PostElement) {
        self.elementIcon.image = element.icon
        self.elementName.text = element.name
    }
    
}
