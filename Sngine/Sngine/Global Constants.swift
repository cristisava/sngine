//
//  Global Constants.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 26/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let facebookDarkBlue = UIColor.rgb(59, green: 89, blue: 152)
    static let facebookLightBlue = UIColor.rgb(139, green: 157, blue: 195)
    static let facebookLightGrey = UIColor.rgb(247, green: 247, blue: 247)
}
