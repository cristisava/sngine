//
//  TimelineSearchBarExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

extension TimelineViewController: UISearchBarDelegate {
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print(searchBar.text ?? "NOTHING")
    }
}
