//
//  CreateNewPostVC.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 28/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class CreateNewPostVC: UIViewController {

    
    
    @IBOutlet weak var tableView: UITableView!
    
    var postElements: [PostElement] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPostElementsToList()
        setupTableView()
        addPostRightButton()
        self.navigationItem.title = "Update Status"
    }

    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        registerCells()
    }
    
    
    func addPostRightButton() {
        let postButton = UIBarButtonItem(title: "Post", style: .done, target: self, action: #selector(CreateNewPostVC.postButtonTapped))
        self.navigationItem.rightBarButtonItem = postButton
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    
    func postButtonTapped() {
        UserAlerts.notifyUserWithBlock("Success", message: "Your post is now visible to the world!", viewController: self) {
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    
    
    func addPostElementsToList() {
        let photoVideo = PostElement(icon: #imageLiteral(resourceName: "photo"), name: "Photo/Video")
        let liveVideo = PostElement(icon: #imageLiteral(resourceName: "live"), name: "Live Video")
        let checkIn = PostElement(icon: #imageLiteral(resourceName: "checkIn"), name: "Check In")
        let feelingActivity = PostElement(icon: #imageLiteral(resourceName: "feeling"), name: "Feeling/Activity")
        let tagFriends = PostElement(icon: #imageLiteral(resourceName: "tagFriends"), name: "Tag Friends")
        postElements.append(photoVideo)
        postElements.append(liveVideo)
        postElements.append(checkIn)
        postElements.append(feelingActivity)
        postElements.append(tagFriends)
        
    }
    
    

}
