//
//  NewPostTVExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 28/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

extension CreateNewPostVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postElements.count + 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let writePostCell = tableView.dequeueReusableCell(withIdentifier: CreatePostCells.writePostCell, for: indexPath) as! WritePostCell
            return writePostCell
        default:
            let postElementsCell = tableView.dequeueReusableCell(withIdentifier: CreatePostCells.postElementsCell, for: indexPath) as! PostElementsCell
            let element = postElements[indexPath.row - 1]
            postElementsCell.setupCell(element: element)
            return postElementsCell
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 305
        default:
            return 50
        }
    }
    
    func registerCells() {
        let writePostNib = UINib(nibName: CreatePostCells.writePostCell, bundle: nil)
        tableView.register(writePostNib, forCellReuseIdentifier: CreatePostCells.writePostCell)
        let postElementNib = UINib(nibName: CreatePostCells.postElementsCell, bundle: nil)
        tableView.register(postElementNib, forCellReuseIdentifier: CreatePostCells.postElementsCell)
    }
    
    
}
