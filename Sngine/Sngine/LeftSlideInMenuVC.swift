//
//  LeftSlideInMenuVC.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class LeftSlideInMenuVC: UIViewController {

    
    
    @IBOutlet weak var tableView: UITableView!
    
    var usersList: [SideMenuModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addDummyData()
    }

    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = SideMenuConstants.darkBackground
        registerCells()
    }
    
    
    func addDummyData() {
        let firstUser = SideMenuModel(profileImage: #imageLiteral(resourceName: "jameshetfield"), statusIcon: #imageLiteral(resourceName: "status_online"), userName: "James Hetfield")
        let secondUser = SideMenuModel(profileImage: #imageLiteral(resourceName: "ionlucacaragiale"), statusIcon: #imageLiteral(resourceName: "status_online"), userName: "Ion Luca Caragiale")
        let thirdUser = SideMenuModel(profileImage: #imageLiteral(resourceName: "steve_profile"), statusIcon: #imageLiteral(resourceName: "status_online"), userName: "Steve Jobs")
        usersList.append(firstUser)
        usersList.append(secondUser)
        usersList.append(thirdUser)
    }
    
    
    
}
