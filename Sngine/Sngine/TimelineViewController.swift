//
//  TimelineViewController.swift
//  Sngine
//
//  Created by CristianSava on 05/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit
import SideMenu

class TimelineViewController: UIViewController {
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var posts = [Post]()
    var rightButton: UIBarButtonItem!
   // lazy   var searchBar:UISearchBar = UISearchBar(frame: CGRect(0, 0, 200, 20))
    lazy var searchBar:UISearchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 200, height: 20))
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        addAccountSettingsButton()
        posts = TimelineInteractor.addDummyData()
        setupNavigationBar()
        setupCollectionView()
        setupSideMenu()
        addSearchBarToNavigationBar(viewController: self)
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func addSearchBarToNavigationBar(viewController: UIViewController) {
        searchBar.placeholder = TimelineConstants.searchBarPlaceholder
        searchBar.delegate = self
        for view in searchBar.subviews {
            for subview in view.subviews {
                if subview.isKind(of: UITextField.self) {
                    let textField: UITextField = subview as! UITextField
                    textField.textColor = UIColor.white
                    textField.backgroundColor = UIColor.rgb(41, green: 63, blue: 104)
                    let attributeDict = [NSForegroundColorAttributeName: UIColor.lightText]
                    textField.attributedPlaceholder = NSAttributedString(string: TimelineConstants.searchBarPlaceholder, attributes: attributeDict)
                }
            }
        }
        viewController.navigationItem.titleView = searchBar
    }
    
    
    func setupNavigationBar() {
        navigationItem.title = TimelineConstants.navigationBarTitle
        navigationItem.setRightBarButton(rightButton, animated: true)
        navigationItem.rightBarButtonItem?.tintColor = UIColor.white

    }
    
    
    func setupCollectionView() {
        collectionView?.alwaysBounceVertical = true
        collectionView?.backgroundColor = TimelineConstants.collectionViewBGColor
        collectionView?.register(FeedCell.self, forCellWithReuseIdentifier: CellsConstants.feedCellId)
        let nib = UINib(nibName: TimelineConstants.whatsOnYourMindCell, bundle: nil)
        collectionView?.register(nib, forCellWithReuseIdentifier: TimelineConstants.whatsOnYourMindCell)
    }
    
    
    func addAccountSettingsButton() {
        rightButton = UIBarButtonItem(image: #imageLiteral(resourceName: "account-settings"), style: .plain, target: self, action: #selector(TimelineViewController.accountSettingsButtonTapped))
    }
    
    
    func setupSideMenu() {
        let sideMenu = LeftSlideInMenuVC(nibName: TimelineConstants.leftSlideInMenuVC, bundle: nil)
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenu)
        menuLeftNavigationController.leftSide = true
        SideMenuManager.menuAnimationBackgroundColor = Colors.facebookDarkBlue
        SideMenuManager.menuLeftNavigationController?.navigationBar.barTintColor = SideMenuConstants.darkBackground
    menuLeftNavigationController.setNavigationBarHidden(true, animated: true)
        SideMenuManager.menuParallaxStrength = 5
        SideMenuManager.menuPresentMode = SideMenuManager.MenuPresentMode(rawValue: 0)!
        SideMenuManager.menuLeftNavigationController = menuLeftNavigationController
    }
    
    
    func accountSettingsButtonTapped() {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
}




