//
//  TimelineInteractor.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 12/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit

class TimelineInteractor {
    
    
    
    static func addDummyData() -> [Post] {
        var posts = [Post]()
        let postMark = Post()
        postMark.name = "Cristi Sava"
        postMark.profileImageName = "cristisava"
        postMark.statusText = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English."
        postMark.statusImageUrl = "https://peterlengyel.files.wordpress.com/2014/10/dsc_7066.jpg"
        postMark.numLikes = 400
        postMark.numComments = 123
        
        let postSteve = Post()
        postSteve.name = "James Hetfield"
        postSteve.profileImageName = "jameshetfield"
        postSteve.statusText = "Authority pisses me off. I think everyone should be able to drink and get loud whenever they want."
        postSteve.statusImageUrl = "https://preview.ibb.co/f3n91Q/metallica.jpg"
        postSteve.numLikes = 1000
        postSteve.numComments = 55
        
        let postGandhi = Post()
        postGandhi.name = "Ion Luca Caragiale"
        postGandhi.profileImageName = "ionlucacaragiale"
        postGandhi.statusText = "„Nu sunt turmentat, dar eu... Eu pentru cine votez? (un cetățean turmentat, O scrisoare pierdută - 1884)” — Ion Luca Caragiale"
        postGandhi.statusImageUrl = "https://preview.ibb.co/koae1Q/ion_blestemul_mic_large.jpg"
        postGandhi.numLikes = 333
        postGandhi.numComments = 22
        
        
        posts.append(postMark)
        posts.append(postSteve)
        posts.append(postGandhi)
        return posts
    }
    
}
