//
//  Settings.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class Settings {
    var name: String
    var image: UIImage
    
    
    init(name: String, image: UIImage) {
        self.name = name
        self.image = image
    }
}
