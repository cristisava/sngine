//
//  SettingsVC.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class SettingsVC: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var settingsArray: [Settings] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        addSettingsRows()
    }

    func setupTableView () {
        tableView.delegate = self
        tableView.dataSource = self
        registerSettingsUserCell()
        registerSettingsCell()
        tableView.contentInset = UIEdgeInsetsMake(-35.0, 0.0, 0.0, 0.0)
    }
    
    
    func addSettingsRows() {
       let friends = Settings(name: "Friends", image: #imageLiteral(resourceName: "friends"))
        let events = Settings(name: "Events", image: #imageLiteral(resourceName: "events"))
        let groups = Settings(name: "Groups", image: #imageLiteral(resourceName: "groups"))
        let shops = Settings(name: "Shops", image: #imageLiteral(resourceName: "shops"))
        let nearby = Settings(name: "Nearby Places", image: #imageLiteral(resourceName: "nearby"))
        let onThisDay = Settings(name: "On this day", image: #imageLiteral(resourceName: "onThisDay"))
        let sports = Settings(name: "Sports", image: #imageLiteral(resourceName: "sports"))
        let pages = Settings(name: "Pages", image: #imageLiteral(resourceName: "pages"))
        settingsArray.append(friends)
        settingsArray.append(events)
        settingsArray.append(groups)
        settingsArray.append(shops)
        settingsArray.append(nearby)
        settingsArray.append(onThisDay)
        settingsArray.append(sports)
        settingsArray.append(pages)
    }
    

}
