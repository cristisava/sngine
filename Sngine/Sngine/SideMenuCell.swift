//
//  SideMenuCell.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    
    
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var userStatusImageView: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = SideMenuConstants.darkBackground
        // Initialization code
    }

    func setupCell(item: SideMenuModel) {
        self.userImageView.image = item.profileImage
        self.userStatusImageView.image = item.statusIcon
        self.userNameLabel.text = item.userName
    }
    
}
