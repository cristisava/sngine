//
//  UserAlerts.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 25/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit


struct UserAlerts {
    
    
    static func notifyUser(_ title: String, message: String, viewController: UIViewController) -> Void {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK",
                                         style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
    
    
    static func notifyUserWithBlock(_ title: String, message: String, viewController: UIViewController, completion: @escaping ()->()) -> Void {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: UIAlertControllerStyle.alert)
        let cancelAction = UIAlertAction(title: "OK", style: .default) { (action) in
            completion()
        }
        alert.addAction(cancelAction)
        viewController.present(alert, animated: true, completion: nil)
    }
}
