//
//  RegisterVC.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 25/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class RegisterVC: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var surnameTextField: UITextField!
    
    @IBOutlet weak var emailOrPhoneTextField: UITextField!
    
    @IBOutlet weak var repeatEmailOrPhoneTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var femaleButton: UIButton!
    
    @IBOutlet weak var maleButton: UIButton!
    
    @IBOutlet weak var createAccountButton: UIButton!
    
    
    
    var femaleSelected = false
    var maleSelected = false
    var textFields: [UITextField] = []
    
    func createSeparator() -> UIView {
        let line = UIView()
        line.backgroundColor = UIColor.white
        return line
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGenderButtonsToUnchecked()
        addBottomSeparatorToTextfields()
        
    }
    
    
    func setGenderButtonsToUnchecked() {
        femaleButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        maleButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
    }
    
    
    func addBottomSeparatorToTextfields() {
        textFields = [firstNameTextField, surnameTextField, emailOrPhoneTextField, repeatEmailOrPhoneTextField, passwordTextField]
        for textfield in textFields {
            let bottomSeparator = createSeparator()
            textfield.attributedPlaceholder = NSAttributedString(string: textfield.placeholder!, attributes: [NSForegroundColorAttributeName: UIColor.lightText])
            textfield.borderStyle = .none
            bottomSeparator.frame = CGRect(x: textfield.bounds.origin.x, y: textfield.frame.size.height, width: textfield.frame.size.width, height: 0.5)
            print(bottomSeparator.frame.origin)
            textfield.addSubview(bottomSeparator)
        }
    }
    
    func highlightCreateAccountButton() {
        if !textFieldsIsEmpty() {
            createAccountButton.setTitleColor(UIColor.white, for: .normal)
        } else {
            createAccountButton.setTitleColor(UIColor.lightText, for: .normal)
        }
    }
    
    
    func textFieldsIsEmpty() -> Bool {
        var isEmpty = false
        for textField in textFields {
            if (textField.text?.isEmpty)! {
                isEmpty = true
            } else {
                isEmpty = false
            }
        }
        return isEmpty
    }
    
    
    func mailOrPhoneIsEqual() -> Bool {
        if emailOrPhoneTextField.text == repeatEmailOrPhoneTextField.text {
            return true
        } else {
            return false
        }
    }
    
    
    @IBAction func femaleButtonPressed(_ sender: UIButton) {
        switch femaleSelected {
        case false:
            femaleButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            femaleSelected = true
            maleSelected = false
            highlightCreateAccountButton()
            maleButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        default:
            break
        }
    }
    
    
    @IBAction func maleButtonPressed(_ sender: UIButton) {
        switch maleSelected {
        case false:
            maleButton.setImage(#imageLiteral(resourceName: "checked"), for: .normal)
            maleSelected = true
            femaleSelected = false
            highlightCreateAccountButton()
            femaleButton.setImage(#imageLiteral(resourceName: "unchecked"), for: .normal)
        default:
            break
        }
    }
    
    

    @IBAction func createAccountButtonPressed(_ sender: Any) {
        if femaleSelected || maleSelected {
            switch (textFieldsIsEmpty(), mailOrPhoneIsEqual()) {
            case (true, _):
                UserAlerts.notifyUser("Notice", message: "Please fill all fields!", viewController: self)
            case (false, false):
                UserAlerts.notifyUser("Notice", message: "Please enter your mail/phone correctly!", viewController: self)
            case (false, true):
                UserAlerts.notifyUser("User created", message: "Your account is now ready to use", viewController: self)
            }
        }
    }
    
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        let loginVC = LoginViewController(nibName: "LoginViewController", bundle: nil)
        self.present(loginVC, animated: true, completion: nil)
    }
    
}
