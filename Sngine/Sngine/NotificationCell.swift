//
//  NotificationCell.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 26/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = Colors.facebookLightGrey
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
