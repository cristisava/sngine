//
//  LoginViewController.swift
//  Sngine
//
//  Created by CristianSava on 04/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isHidden = true
    }
    @IBAction func pressedLoginButton(_ sender: Any) {
        self.present(CustomTabBarController(), animated: true, completion: nil)
    }
    
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        let registerVC = RegisterVC(nibName: "RegisterVC", bundle: nil)
        self.present(registerVC, animated: true, completion: nil)
    }
    
    
}
