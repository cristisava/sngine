//
//  CreateNewPostConstants.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 28/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

struct CreatePostCells {
    static let writePostCell = "WritePostCell"
    static let postElementsCell = "PostElementsCell"
}
