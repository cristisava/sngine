//
//  TimelineConstants.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 12/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit

struct TimelineConstants {
    static let navigationBarTitle = "Sngine feeds"
    static let collectionViewBGColor = UIColor(white: 0.95, alpha: 1)
    static let searchBarPlaceholder = "Search"
    static let leftSlideInMenuVC = "LeftSlideInMenuVC"
    static let whatsOnYourMindCell = "WhatsOnYourMindCell"
}
