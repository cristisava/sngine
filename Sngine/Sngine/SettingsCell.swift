//
//  SettingsCell.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 27/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class SettingsCell: UITableViewCell {

    
    @IBOutlet weak var settingsIcon: UIImageView!
    
    @IBOutlet weak var settingsLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setupCell(item: Settings) {
        settingsIcon.image = item.image
        settingsLabel.text = item.name
        settingsIcon.layer.masksToBounds = true
        settingsIcon.layer.cornerRadius = 5
    }
    
}
