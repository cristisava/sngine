//
//  AppDelegate.swift
//  Sngine
//
//  Created by CristianSava on 03/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        AppController.launchDashboardIn(window: window!)
        return true
    }

    

}

