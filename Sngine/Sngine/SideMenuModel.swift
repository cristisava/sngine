//
//  SideMenuModel.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class SideMenuModel {
    var profileImage: UIImage
    var statusIcon: UIImage
    var userName: String
    
    init(profileImage: UIImage, statusIcon: UIImage, userName: String) {
        self.profileImage = profileImage
        self.statusIcon = statusIcon
        self.userName = userName
    }
}
