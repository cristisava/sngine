//
//  SearchAndSettingsCell.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 30/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class SearchAndSettingsCell: UITableViewCell {

    @IBOutlet weak var searchBarOutlet: UISearchBar!
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.backgroundColor = SideMenuConstants.darkBackground
        searchBarOutlet.tintColor = SideMenuConstants.darkBackground
        searchBarOutlet.backgroundColor = SideMenuConstants.darkBackground
        searchBarOutlet.barTintColor = SideMenuConstants.darkBackground
        for view in searchBarOutlet.subviews {
            for subview in view.subviews {
            if subview.isKind(of: UITextField.self) {
                subview.backgroundColor = UIColor.rgb(40, green: 44, blue: 52)
            }
        }
    }
    }
    
    
}
