//
//  CustomTabBarController.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 11/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class CustomTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let feedController = TimelineViewController()
        let navigationController = UINavigationController(rootViewController: feedController)
        navigationController.title = "News Feed"
        navigationController.tabBarItem.image = #imageLiteral(resourceName: "news_feed_icon")
        
        let friendRequestsController = FriendRequestsController()
        let secondNavigationController = UINavigationController(rootViewController: friendRequestsController)
        secondNavigationController.title = "Requests"
        secondNavigationController.tabBarItem.image = #imageLiteral(resourceName: "requests_icon")
        
        let messengerVC = UIViewController()
        messengerVC.navigationItem.title = "SOME TITLE"
        let messengerNavigationController = UINavigationController(rootViewController: messengerVC)
        messengerNavigationController.title = "Messenger"
        messengerNavigationController.tabBarItem.image = #imageLiteral(resourceName: "messenger_icon")
        
        let notificationsVC = NotificationsVC()
        notificationsVC.navigationItem.title = "Notifications"
        let notificationsNavController = UINavigationController(rootViewController: notificationsVC)
        notificationsNavController.title = "Notifications"
        notificationsNavController.tabBarItem.image = #imageLiteral(resourceName: "globe_icon")
        
        let settingsVC = SettingsVC()
        settingsVC.navigationItem.title = "Settings"
        let moreNavController = UINavigationController(rootViewController: settingsVC)
        moreNavController.title = "Settings"
        moreNavController.tabBarItem.image = #imageLiteral(resourceName: "more_icon")
        
        viewControllers = [navigationController, secondNavigationController, messengerNavigationController, notificationsNavController, moreNavController]
        
        tabBar.isTranslucent = false
        
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0, y: 0, width: 1000, height: 0.5)
        topBorder.backgroundColor = UIColor.rgb(229, green: 231, blue: 235).cgColor
        
        tabBar.layer.addSublayer(topBorder)
        tabBar.clipsToBounds = true
        
    }
}
