//
//  PostElement.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 28/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import UIKit

class PostElement {
    
    var icon: UIImage
    var name: String
    
    init(icon: UIImage, name: String) {
        self.icon = icon
        self.name = name
    }
    
}
