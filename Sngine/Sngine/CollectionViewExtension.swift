//
//  CollectionViewExtension.swift
//  Sngine
//
//  Created by Mugurel Moscaliuc on 11/04/2017.
//  Copyright © 2017 Sngine. All rights reserved.
//

import Foundation
import UIKit

extension TimelineViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, PostButtonsDelegate {
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
        case 0:
            let whatsOnYourMindCell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhatsOnYourMindCell", for: indexPath) as! WhatsOnYourMindCell
            return whatsOnYourMindCell
        default:
            let feedCell = collectionView.dequeueReusableCell(withReuseIdentifier: CellsConstants.feedCellId, for: indexPath) as! FeedCell
            feedCell.delegate = self
            addTapGestureToUserImage(cell: feedCell)
            feedCell.post = posts[indexPath.item]
            return feedCell
            
        }
    }
    
    func addTapGestureToUserImage(cell: FeedCell) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapUserName(_:)))
        cell.profileImageView.addGestureRecognizer(tapGesture)
        cell.profileImageView.isUserInteractionEnabled = true
    }
    
    
    func tapUserName(_ sender: UITapGestureRecognizer) {
        let userProfileVC = UserProfileVC(nibName: "TimelineViewController", bundle: nil)
        self.navigationController?.pushViewController(userProfileVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.item {
        case 0:
            return CGSize(width: UIScreen.main.bounds.width, height: 70)
        default:
            if let statusText = posts[indexPath.item].statusText {
                
                let rect = NSString(string: statusText).boundingRect(with: CGSize(width: view.frame.width, height: 1000), options: NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin), attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14)], context: nil)
                
                let knownHeight: CGFloat = 8 + 44 + 4 + 4 + 200 + 8 + 24 + 8 + 44
                
                return CGSize(width: view.frame.width, height: rect.height + knownHeight + 24)
            }
            
            return CGSize(width: view.frame.width, height: 500)
        }
    }
    
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.item {
        case 0:
            let createPostVC = CreateNewPostVC(nibName: "CreateNewPostVC", bundle: nil)
            self.navigationController?.pushViewController(createPostVC, animated: true)
        default:
            print("Show the proper post to user")
        }
    }
    
    
    func userDidTapButton(withName: String, cell: FeedCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else {return}
        let post = posts[indexPath.item]
        switch withName {
        case "Like" , "Unlike":
            likeOrUnlike(isLiked: post.isLiked, cell: cell, post: post, indexPath: indexPath.item)
        case "Comment":
            post.numComments = post.numComments! + 1
            collectionView?.reloadData()
        case "Share":
            print("You can write it on the paper meanwhile")
        default:
            break
        }
    }
    
    
    func likeOrUnlike(isLiked: Bool, cell: FeedCell, post: Post, indexPath: Int) {
        if !isLiked {
            post.numLikes = post.numLikes! + 1
            cell.likeButton.setTitle("Unlike", for: .normal)
            cell.likeButton.setTitleColor(UIColor.rgb(140, green: 90, blue: 149), for: .normal)
            cell.post?.isLiked = true
            posts.insert(post, at: indexPath)
            posts.remove(at: indexPath + 1)
            collectionView?.reloadData()
        } else if isLiked {
            post.numLikes = post.numLikes! - 1
            cell.likeButton.setTitle("Like", for: .normal)
            cell.likeButton.setTitleColor(UIColor.rgb(143, green: 150, blue: 163), for: .normal)
            cell.post?.isLiked = false
            posts.insert(post, at: indexPath)
            posts.remove(at: indexPath + 1)
            collectionView?.reloadData()
        }
    }
    
    
}
